import MainPill from './main-pill'

describe('@components/main-pill', () => {
  it('exports a valid component', () => {
    expect(MainPill).toBeAComponent()
  })
})

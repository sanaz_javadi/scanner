import QrCode from './qr-code'

describe('@components/qr-code', () => {
  it('exports a valid component', () => {
    expect(QrCode).toBeAComponent()
  })
})

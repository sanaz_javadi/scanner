import DashboardLayoutLayout from './dashboardLayout'

describe('@layouts/dashboardLayout', () => {
  it('renders its content', () => {
    const slotContent = '<p>Hello!</p>'
    const { element } = shallowMount(DashboardLayoutLayout, {
      slots: {
        default: slotContent,
      },
    })
    expect(element.innerHTML).toContain(slotContent)
  })
})

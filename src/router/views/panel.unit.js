import Panel from './panel'

describe('@views/panel', () => {
  it('is a valid view', () => {
    expect(Panel).toBeAViewComponent()
  })
})
